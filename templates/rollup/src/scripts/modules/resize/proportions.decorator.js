import { attach, detach, update } from "../utils/decorator.utils.js";
import resizeStore from "./resize.store.js";

const name = "proportions"; // указать дефолтное имя декоратора

if (name === null) {
    console.error("decorator name is missing");
}

const defaultOptions = {
    name: name,
    maxWidth: Infinity,
    maxHeight: Infinity,
    proportion: 1
};

class Decorator {
    constructor(parent, options) {
        this._parent = parent;
        this._options = options;
        this.handleResize = this.handleResize.bind(this);
    }

    init() {
        resizeStore.subscribe(this.handleResize);
    }

    destroy() {
        resizeStore.unsubscribe(this.handleResize);
    }

    handleResize() {
        this._parent.style.width = "";
        this._parent.style.height = "";;

        let w = this._parent.parentNode.clientWidth;
        let h = this._parent.parentNode.clientHeight;

        w = Math.min(this._options.maxWidth, w);
        h = Math.min(this._options.maxHeight, h);

        if (w / h > this._options.proportion) {
            w = h * this._options.proportion;
        } else if (w / h < this._options.proportion) {
            h = w / this._options.proportion;
        }

        this._parent.style.width = w + "px";
        this._parent.style.height = h + "px";

        if (this._parent.handleResize) {
            this._parent.handleResize();
        }
    }
}

export default {
    attach: (parent, options) => {
        return attach(
            Decorator,
            parent,
            Object.assign({}, defaultOptions, options)
        );
    },
    detach: (parent, options) => {
        return detach(parent, Object.assign({}, defaultOptions, options));
    },
    update: (parent, options) => {
        return update(parent, options);
    },
};
