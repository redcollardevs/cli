import dispatcher from "../dispatcher.js";
import popupStore from "./popup.store.js";

class PopupBack extends HTMLButtonElement {
  constructor(self) {
    self = super(self);
    self.init.call(self);
  }

  handleClick() {
    if (this.previousPopup) {
      dispatcher.dispatch({
        type: "popup:open",
        id: this.previousPopup,
      });
    } else {
      dispatcher.dispatch({
        type: "popup:close-all",
      });
    }
  }

  handlePopups() {
    this.previousPopup = this.currentPopup;
    this.currentPopup = popupStore.getData().active;
  }

  init() {
    this.previousPopup = null;
    this.currentPopup = null;

    this.handleClick = this.handleClick.bind(this);
    this.handlePopups = this.handlePopups.bind(this);
  }

  connectedCallback() {
    popupStore.subscribe(this.handlePopups);
    this.addEventListener("click", this.handleClick);
  }

  disconnectedCallback() {
    popupStore.unsubscribe(this.handlePopups);
    this.removeEventListener("click", this.handleClick);
  }
}

customElements.define("popup-back", PopupBack, {
  extends: "button",
});

export default customElements;
