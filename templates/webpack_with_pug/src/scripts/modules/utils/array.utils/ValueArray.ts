// push({name: 'name', value: 1})
// remove({name: 'name'})
// set({name: 'name', value: 1})

import addArrays from "./addArrays.js";
import addObjects from "../object.utils/addObjects";
import multiplyArrays from "./multiplyArrays.js";
import multiplyObjects from "../object.utils/multiplyObjects";
import { ArrayFunc } from "./arrayTypes";

type valueType = number | Array<number> | Record<string, number>;

type arrayItem = {
    [index: string]: valueType | string | boolean;
    name: string;
    value: valueType;
    ignored: boolean;
};

type Options = {
    includeIgnored: boolean;
};

interface ValueArrayInterface {
    elements: Array<arrayItem>;
    forEach(func: ArrayFunc): void;
    push(value: arrayItem): void;
    remove(value: arrayItem): void;
    set(value: arrayItem): void;
    get(value: arrayItem): void;
    sum(options: Options): void;
    sumExcept(value: arrayItem, options: Options): valueType;
    getExcept(value: arrayItem, options: Options): valueType;
    multiply(options: Options): valueType;
}

class ValuesArray implements ValueArrayInterface {
    elements: Array<arrayItem>;

    constructor() {
        this.elements = [];
    }

    forEach(func: ArrayFunc): void {
        return this.elements.forEach(func);
    }

    push(val: arrayItem): void {
        if (
            this.elements.find(function (el) {
                return el.name === val.name;
            })
        ) {
            throw new Error(`element with name ${val.name} already exists`);
        }

        this.elements.push(val);
    }

    remove(val: arrayItem): void {
        this.elements = this.elements.filter((el) => {
            return el.name !== val.name;
        });
    }

    set(val: arrayItem): void {
        const test: arrayItem | undefined = this.elements.find((el) => {
            return el.name === val.name;
        });

        if (test) {
            test.value = val.value;
        } else {
            throw new Error(`element with name ${val.name} is not found`);
        }
    }

    get(val: arrayItem): unknown {
        if (!val) {
            throw new Error("no value given");
        } else {
            const element = this.elements.find((el) => {
                return el.name === val.name;
            });

            if (element) {
                return element.value;
            } else {
                return null;
            }
        }
    }

    sum(options: Options): valueType {
        options = Object.assign(
            {
                includeIgnored: false,
            },
            options
        );

        return this.elements
            .filter((el) => {
                return options.includeIgnored || !el.ignored;
            })
            .map((el) => {
                return el.value;
            })
            .reduce(
                (total, value): valueType => {
                    if (Array.isArray(value) && Array.isArray(total)) {
                        return addArrays(total, value);
                    } else if (
                        typeof value === "object" &&
                        typeof total === "object"
                    ) {
                        return addObjects(
                            total as Record<string, number>,
                            value as Record<string, number>
                        );
                    } else {
                        return (total as number) + (value as number);
                    }
                }
            );
    }

    sumExcept(val: arrayItem, options: Options): valueType {
        options = Object.assign(
            {
                includeIgnored: false,
            },
            options
        );

        const filtered = this.elements.filter((el) => {
            return (
                (options.includeIgnored || !el.ignored) && el.name !== val.name
            );
        });

        if (filtered.length === 0) {
            const value = this.elements[0].value;

            if (Array.isArray(value)) {
                return new Array(value.length);
            } else {
                const result: { [index: string]: number } = {};

                Object.keys(value).forEach((key) => {
                    result[key] = 0;
                });

                return result;
            }
        } else {
            return filtered
                .filter((el) => {
                    return options.includeIgnored || !el.ignored;
                })
                .map(function (el) {
                    return el.value;
                })
                .reduce((total, value) => {
                    if (Array.isArray(value) && Array.isArray(total)) {
                        return addArrays(total, value);
                    } else if (typeof value === "object") {
                        return addObjects(
                            total as Record<string, number>,
                            value as Record<string, number>
                        );
                    } else {
                        return (total as number) + (value as number);
                    }
                });
        }
    }

    getExcept(val: arrayItem, options: Options): valueType {
        options = Object.assign(
            {
                includeIgnored: false,
            },
            options
        );

        return this.sumExcept(val, options);
    }

    multiply(options: Options): valueType {
        options = Object.assign(
            {
                includeIgnored: false,
            },
            options
        );

        return this.elements
            .filter((el) => {
                return options.includeIgnored || !el.ignored;
            })
            .map(function (val) {
                return val.value;
            })
            .reduce((total, value) => {
                if (Array.isArray(value) && Array.isArray(total)) {
                    return multiplyArrays(total, value);
                } else if (typeof value === "object") {
                    return multiplyObjects(
                        total as Record<string, number>,
                        value as Record<string, number>
                    );
                } else {
                    return (total as number) * (value as number);
                }
            });
    }
}

export default ValuesArray;
