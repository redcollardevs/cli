import { DecoratorClass, DecoratorOptions } from "./decorator.types";
import { Component } from "../component.utils/component.types";

const attachPromise = (
    Decorator: typeof DecoratorClass,
    parent: Component,
    options: DecoratorOptions
): Promise<DecoratorClass> => {
    return new Promise((resolve, reject) => {
        const name = options.name;
        let decorator: DecoratorClass;

        if (!name) {
            reject("decorator has to provide name option");
        }

        if (!parent._decorators) {
            parent._decorators = {};
        }

        if (parent._decorators[name]) {
            decorator = parent._decorators[name];
            resolve(decorator);
        } else {
            decorator = new Decorator(parent, options);
            parent._decorators[name] = decorator;

            Promise.resolve(decorator.init()).then(() => {
                resolve(decorator);
            });
        }
    });
};

export default attachPromise;
