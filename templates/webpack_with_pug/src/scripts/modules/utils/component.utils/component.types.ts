import { DecoratorClass } from "../decorator.utils/decorator.types";

export type ComponentOptions = {
    [index: string]: any;
    __namespace?: string;
};

export type Component = {
    options: ComponentOptions;
    _decorators?: Record<string, DecoratorClass>;
};
