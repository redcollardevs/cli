let root = document.documentElement;
let styles = getComputedStyle(root);

let tablet = styles.getPropertyValue('--tablet');
let desktop = styles.getPropertyValue('--desktop');

function getSize(st) {
	if (!st) return null;
	return parseInt(st); 
}

const size = {
	tablet: getSize(tablet) || 640,
	desktop: getSize(desktop) || 1000,
};

if (!window._vars) {
	window._vars = {};
}

window._vars.size = size;

export { size };
