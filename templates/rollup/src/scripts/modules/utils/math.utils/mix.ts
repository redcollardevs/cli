export default (value1: number, value2: number, mix: number): number => {
    return value1 + (value2 - value1) * mix;
};
