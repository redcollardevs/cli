Math.linearstep = function(edge0, edge1, x) {
	return Math.clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0); 
}

export default Math.linearstep;