class Loop {
    constructor(callback) {
        this.callback = callback;
        
        this.loop = this.loop.bind(this);
        this.start = this.start.bind(this);
        this.kill = this.kill.bind(this);
    }

    loop() {
        if (this._destroy) {
            return;
        }

        this.callback();

        if (!window._masterLoop) {
            requestAnimationFrame(this.loop);
        }
    }

    start() {
        this.loop();

        if (window._masterLoop) {
            window._masterLoop.push(this.loop);
        }
    }

    kill() {
        this._destroy = true;

        if (window._masterLoop) {
            window._masterLoop.remove(this.loop);
        }
    }
}

export default function(callback) {
    if (typeof callback !== "function") {
        console.error("callback has to be a function");
        return;
    }

    return new Loop(callback);
}