export type defaultObject = {
    [index: string]: unknown;
};

export type numbersObject = {
    [index: string]: number;
};
