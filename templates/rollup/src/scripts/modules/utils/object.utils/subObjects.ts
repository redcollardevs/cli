import { defaultObject } from "./objectTypes";

export default (obj1: defaultObject, obj2: defaultObject): defaultObject => {
    const newObject: defaultObject = {};

    Object.keys(obj1).forEach((key) => {
        if (typeof obj1[key] !== "number" || typeof obj2[key] !== "number") {
            return;
        }

        newObject[key] = (obj1[key] as number) - (obj2[key] as number);
    });

    return Object.assign({}, obj2, newObject);
};
