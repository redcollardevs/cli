import mix from "../math.utils/mix";
import { defaultObject, numbersObject } from "./objectTypes";

export default (
    obj1: defaultObject,
    obj2: defaultObject,
    mixValue: number
): numbersObject => {
    const newObject: numbersObject = {};

    Object.keys(obj1).forEach((key) => {
        if (typeof obj1[key] !== "number" || typeof obj2[key] !== "number") {
            return;
        }

        newObject[key] = mix(
            obj1[key] as number,
            obj2[key] as number,
            mixValue
        );
    });

    return Object.assign({}, obj2, newObject);
};
