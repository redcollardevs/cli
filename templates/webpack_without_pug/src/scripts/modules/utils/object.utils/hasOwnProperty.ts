type UnknownObject = {
    [key: string]: unknown;
};

const hasOwnProperty = (
    object: UnknownObject,
    key: string | number
): boolean => {
    return Object.prototype.hasOwnProperty.call(object, key);
};

export default hasOwnProperty;
