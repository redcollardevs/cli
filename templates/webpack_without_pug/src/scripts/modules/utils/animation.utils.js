import transition from './animation.utils/transition.js';
import simpleTween from './animation.utils/simpleTween.js';

export {
	transition,
	simpleTween
}