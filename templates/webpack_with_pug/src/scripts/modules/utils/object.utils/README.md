# Утилитарные скрипты для объектами
___

## deepCopy
___
Возвращает глубокую копию объекта.  
**Синтаксис**
```javaScript
let newObject = deepCopy(oldObject);
```
## mixObjects
___
Смешивает числовые значения объекстов с указанным долевым коеффициентом (0..1)   
**Синтаксис**
```javaScript
let newObject = mixObjects(object1, object2, 0.3);
```