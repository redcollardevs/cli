module.exports = {
  rewrite: [
    {
      from: '/:html',
      to: '/dist/$1'
    }, {
      from: '/static/(.*)',
      to: '/dist/static/$1'
    }, {
      from: '/tmp/(.*)',
      to: '/dist/tmp/$1'
    }, {
      from: '/build/js/index.js',
      to: '/src/scripts/app.js'
    }, {
      from: '/build/js/(.*)',
      to: '/src/scripts/$1'
    }, {
      from: '/build/css/styles.css',
      to: '/src/styles/app.css'
    }, {
      from: '/build/css/(.*)',
      to: '/src/styles/$1'
    }, {
      from: '/build/fonts/(.*)',
      to: '/src/fonts/$1'
    }
  ],
  'verbose.include': 'middleware.rewrite',
  logFormat: 'stats'
}