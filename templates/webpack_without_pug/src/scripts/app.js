import dispatcher from './modules/dispatcher.js';
import domReady from './modules/domReady.js';
import mathUtils from './modules/utils/math.utils.js';
import keyboardEvents from './modules/events/keyboard.view.js';
import resizeHelper from './modules/resize/resize.helper.js';
import scrollHelper from './modules/scroll-helper/scroll.helper.js';
import outliner from './modules/accessibility/outliner.view.js';

import preloaderSimple from './modules/page-load/preloader-simple.view.js';

domReady(function() {
	dispatcher.dispatch({
		type: 'dom:ready'
	});
	
	keyboardEvents.init();
	resizeHelper.init();
	scrollHelper.init();
	preloaderSimple.init();
});