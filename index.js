#!/usr/bin/env node

const inquirer = require('inquirer');
const fs = require('fs');

const CURR_DIR = process.cwd();
let projectName, template;

const questions = [
    {
        name: 'project-name',
        type: 'input',
        message: 'Project name:',
        default: 'my_app',
        validate: function (input) {
            if (/^([A-Za-z\-\_\d])+$/.test(input)) return true;
            else return 'Project name may only include letters, numbers, underscores and hashes.';
        }
    },
    {
        name: 'bundler',
        type: 'list',
        message: 'Choose bundler',
        choices: ['webpack with pug', 'webpack without pug', 'rollup']
    }
];


inquirer.prompt(questions)
    .then(questions => {
        projectName = questions['project-name'];
        template = `${__dirname}/templates/${questions['bundler'].split(' ').join('_')}`;

        fs.mkdirSync(`${CURR_DIR}/${projectName}`);
        createDirectoryContents(template, projectName);
    });

const createDirectoryContents = (template, newProjectPath) => {
    const filesToCreate = fs.readdirSync(template);

    filesToCreate.forEach(file => {
        console.log(file);
        const origFilePath = `${template}/${file}`;

        const stats = fs.statSync(origFilePath);

        if (stats.isFile()) {
            const contents = fs.readFileSync(origFilePath, 'utf8');

            const writePath = `${CURR_DIR}/${newProjectPath}/${file}`;
            fs.writeFileSync(writePath, contents, 'utf8');
        } else if (stats.isDirectory()) {
            fs.mkdirSync(`${CURR_DIR}/${newProjectPath}/${file}`);

            createDirectoryContents(`${template}/${file}`, `${newProjectPath}/${file}`);
        }
    });
};
