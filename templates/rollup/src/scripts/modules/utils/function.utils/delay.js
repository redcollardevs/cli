let delay = function(f, ms) {
    return function () {
        let savedThis = this;
        let savedArgs = arguments;

        setTimeout(() => {
            f.apply(savedThis, savedArgs);
        }, ms);
    };
};

export default delay;