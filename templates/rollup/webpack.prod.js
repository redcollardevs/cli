const webpack = require('webpack');
const CopyPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const path = require('path');

module.exports = {
    mode: 'production',

    entry: __dirname + '/src/entry.js',

    output: {
        filename: 'index.js',
        path: path.resolve(__dirname, 'dist/build'),
    },

    resolve: {
        extensions: [],
        modules: []
    },

    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {}
                    },
                    'css-loader'
                ]
            }, {
                test: /\.woff2?$|\.ttf$|\.eot$/,
                loader: 'file-loader?name=./fonts/[hash].[ext]'
            }, {
                test: /\.svg$|\.png|\.webp|\.jpe?g|\.gif$/,
                loader: 'file-loader?name=./images/[hash].[ext]'
            }, {
                test: /\.mp4$|\.webm$|\.mp3$/,
                loader: 'file-loader?name=./video/[hash].[ext]'
            }
        ]
    },

    optimization: {
        minimizer: [
            new OptimizeCssAssetsPlugin()
        ]
    },

    plugins: [
        new MiniCssExtractPlugin({
            filename: 'css/styles.css',
        }),
    ]
};
