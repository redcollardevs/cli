import rootImport from 'rollup-plugin-root-import';

console.log('custom config');

export default {
  input: 'src/scripts/app.js',
  output: {
    file: 'dist/build/js/index.js',
    format: 'iife'
  },
  plugins: [ rootImport({
    root: `${__dirname}`,
    useEntry: 'prepend',
    extensions: '.js'
  }) ]
};